<?php
namespace Sharecoto\FbMessage;

use Illuminate\Support\Collection;

class Entry extends Collection
{
    public function __construct($items = [])
    {
        foreach ($items as $key=>$val) {
            if (isset($val['messaging'])) {
                $items[$key]['messaging'] = new Messaging($val['messaging']);
            }
        }
        parent::__construct($items);
    }
}
