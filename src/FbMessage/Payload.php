<?php
namespace Sharecoto\FbMessage;

use Illuminate\Support\Collection;

class Payload extends Collection
{
    protected $schema;

    public function __construct($items = [])
    {
        if (isset($items['entry'])) {
            $items['entry'] = new Entry($items['entry']);
        }
        parent::__construct($items);
    }

    /**
     * imageを含むmessagingのみ抽出
     *
     * @return Messaging
     */
    public function extractImageMessages()
    {
        $entries = $this['entry']->pluck('messaging')
            ->filter(function($v) {
                return !!$v;
            });
        $messages = $entries->map(function($v, $i) {
            $messages = $v->filter(function($v) {
                if ($v->has('message') && isset($v['message']['attachments'])) {
                    if (isset($v['message']['sticker_id'])) {
                        return false;
                    }
                    return $v['message']['attachments']->contains('type', 'image');
                }
            });
            return ($messages);
        });
        $images = $messages->reduce(function($carry, $v) {
            return $carry->merge($v);
        }, $carry = new Messaging);
        return $images;
    }
}
