<?php

namespace Sharecoto\FbMessage;

use Illuminate\Support\Collection;

class Messaging extends Collection
{
    public function __construct($item = [])
    {
        foreach ($item as $key => $val) {
            if (is_array($val)) {
                $item[$key] = self::newInstance($val);
            }
        }
        parent::__construct($item);
    }

    private static function newInstance($message)
    {
        if (isset($message['message'])) {
            return new Messaging\Message($message);
        }
        
        if (isset($message['delivery'])) {
            return new Messaging\Delivery($message);
        }

        if (isset($message['postback'])) {
            return new Messaging\PostBack($message);
        }

        if (isset($message['optin'])) {
            return new Messaging\Authentication($message);
        }

        return new Messaging\Message($message);
    }
}
