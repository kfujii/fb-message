<?php

namespace Sharecoto\FbMessage\Messaging;

use Illuminate\Support\Collection;

class Message extends Collection
{
    public function __construct($item = [])
    {
        if (isset($item['message']['attachments'])) {
            $item['message']['attachments'] = new Attachment($item['message']['attachments']);
        }
        parent::__construct($item);
    }

    public function getSenderId()
    {
        return $this['sender']['id'];
    }

    public function getRecipientId()
    {
        return $this['recipient']['id'];
    }

    public function getMessage()
    {
        return $this['message'] ?? null;
    }
}
