<?php

require __DIR__ . '/../vendor/autoload.php';

use Sharecoto\FbMessage\Entry;
use Sharecoto\FbMessage\Messaging;
use Sharecoto\FbMessage\Payload;
use Sharecoto\FbMessage\Messaging\Attachment;
use Sharecoto\FbMessage\Messaging\Authentication;
use Sharecoto\FbMessage\Messaging\Delivery;
use Sharecoto\FbMessage\Messaging\Postback;

// instance
$entry = new Entry();
var_dump($entry);

// Messaging Instance
$messaging = new Messaging;
var_dump($messaging);

// Payload Instance
$payload = new Payload();
var_dump($payload);

// Attachment Instance
$attachement = new Attachment;
var_dump($attachement);

// Authentication Instance
$auth = new Authentication;
var_dump($auth);

// Delivery Instance
$delivery = new Delivery;
var_dump($delivery);

$postback = new Postback;
var_dump($postback);
